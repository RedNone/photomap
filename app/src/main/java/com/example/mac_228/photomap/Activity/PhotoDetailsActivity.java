package com.example.mac_228.photomap.Activity;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mac_228.photomap.FireBaseManager;
import com.example.mac_228.photomap.Model.PhotoSendModel;
import com.example.mac_228.photomap.R;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PhotoDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String TAG = "PhotoDetailsActivity";


    public final int PHOTO_PREVIEW = 1;

    private Toolbar toolbar;

    public static final int NEW_IMAGE_BUTTON = 1;
    public static final int NEW_IMAGE_LONGPRESS = 2;
    public static final int EXISTING_PHOTO = 3;

    private final int GALLERY_TYPE = 0;
    private final int CAMERA_TYPE = 1;

    private int mIdOfExistingPhoto;
    private FireBaseManager mManager;
    private PhotoSendModel mObjectOfExistingPhoto;

    public static final String TYPE_OF_IMAGE = "typeImage";
    public static final String NEW_IMAGE_URI = "newImageUri";
    public static final String NEW_IMAGE_LOCATION = "newImageLocation";
    public static final String EXISTING_PHOTO_ID = "existingPhotoId";

    private boolean mTypeOfPhoto;

    private ImageView mImage;
    private TextView mTime, mType;
    private EditText mDescription;

    private String coordinats = "";

    private String mDate = "";

    private Uri newImageUri = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_details);
        toolbar = (Toolbar) findViewById(R.id.photoToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(null);

        mManager = FireBaseManager.getInstance();

        mTime = (TextView) findViewById(R.id.timeTextView);
        mType = (TextView) findViewById(R.id.typeTextView);

        mDescription = (EditText) findViewById(R.id.descriptionEditText);

        mImage = (ImageView) findViewById(R.id.imageViewPhotoDetails);
        mImage.setOnClickListener(this);

        registerForContextMenu(mType);

        checkData();

        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_arrow_left));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

    }

    private void checkData() {
        Intent intent = getIntent();
        if (intent.getIntExtra(TAG, 0) == NEW_IMAGE_BUTTON) {

            mType.setText(R.string.friends);

            mDate = getDate();

            mTime.setText(mDate);

            coordinats = intent.getStringExtra(NEW_IMAGE_LOCATION);

            if (intent.getIntExtra(TYPE_OF_IMAGE, 0) == GALLERY_TYPE) {
                newImageUri = Uri.parse(intent.getStringExtra(NEW_IMAGE_URI));
                mImage.setImageURI(newImageUri);
            }
            if (intent.getIntExtra(TYPE_OF_IMAGE, 0) == CAMERA_TYPE) {
                newImageUri = Uri.parse(intent.getStringExtra(NEW_IMAGE_URI));
                mImage.setImageURI(newImageUri);
            }
            mTypeOfPhoto = false;
        }

        if (intent.getIntExtra(TAG, 0) == NEW_IMAGE_LONGPRESS) {
            mType.setText(R.string.friends);

            mDate = getDate();

            mTime.setText(mDate);

            coordinats = intent.getStringExtra(NEW_IMAGE_LOCATION);

            if (intent.getIntExtra(TYPE_OF_IMAGE, 0) == GALLERY_TYPE) {
                newImageUri = Uri.parse(intent.getStringExtra(NEW_IMAGE_URI));
                mImage.setImageURI(newImageUri);
            }
            if (intent.getIntExtra(TYPE_OF_IMAGE, 0) == CAMERA_TYPE) {
                newImageUri = Uri.parse(intent.getStringExtra(NEW_IMAGE_URI));
                mImage.setImageURI(newImageUri);
            }
            mTypeOfPhoto = false;
        }

        if (intent.getIntExtra(TAG, 0) == EXISTING_PHOTO) {
            mIdOfExistingPhoto = Integer.valueOf(intent.getStringExtra(EXISTING_PHOTO_ID));
            mObjectOfExistingPhoto = mManager.getNewDataList().get(mIdOfExistingPhoto);
            mType.setText(mObjectOfExistingPhoto.getType().toString());
            mTime.setText(mObjectOfExistingPhoto.getTime());

            if (!TextUtils.isEmpty(mObjectOfExistingPhoto.getText())) {
                mDescription.setText(mObjectOfExistingPhoto.getText());
            }
            newImageUri = mObjectOfExistingPhoto.getUri();
            mImage.setImageURI(newImageUri);
            mTypeOfPhoto = true;
        }

    }

    private String getDate() {
        SimpleDateFormat fmt = new SimpleDateFormat("MMMM dd'th',yyyy - hh:mm a");
        Date date = new Date();

        return fmt.format(date);

    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {


        switch (item.getItemId()) {
            case R.id.friends_menu:
                mType.setText(R.string.friends);
                break;
            case R.id.nature_menu:
                mType.setText(R.string.nature);
                break;
            case R.id.defualt_menu:
                mType.setText(R.string.default_type);
                break;
            case R.id.food_menu:
                mType.setText(R.string.food);
                break;

        }
        return super.onContextItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imageViewPhotoDetails:
                startActivity(prepareIntent());
                break;
        }
    }

    private Intent prepareIntent() {
        Intent intent = new Intent(this, FullPhotoActivity.class);
        intent.putExtra(FullPhotoActivity.DATE_EXTRA, mDate);

        String str = mDescription.getText().toString();

        if (!TextUtils.isEmpty(str)) {
            intent.putExtra(FullPhotoActivity.DISCRIPTION_EXTRA, str);
        } else {
            intent.putExtra(FullPhotoActivity.DISCRIPTION_EXTRA, "");
        }

        intent.putExtra(FullPhotoActivity.IMAGE_URI, newImageUri.toString());

        return intent;
    }

    private PhotoSendModel newPhoto() {
        PhotoSendModel newPhoto = null;
        newPhoto = new PhotoSendModel(
                0,
                newImageUri,
                mDate,
                mType.getText().toString(),
                mDescription.getText().toString(),
                coordinats
        );
        return newPhoto;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        if (id == R.id.action_accept) {
            if (mTypeOfPhoto) {
                mManager.updateData(mIdOfExistingPhoto, mType.getText().toString(), mDescription.getText().toString());
            } else {
                mManager.sendData(newPhoto());
            }
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }


}
