package com.example.mac_228.photomap.Model;

import android.net.Uri;


public class PhotoSendModel {

    private int id;
    private Uri uri;
    private String time;
    private String type;
    private String text;
    private String latlng;

    public PhotoSendModel(int id, Uri uri, String time, String type, String text, String latlng) {
        this.id = id;
        this.uri = uri;
        this.time = time;
        this.type = type;
        this.text = text;
        this.latlng = latlng;
    }

    public PhotoSendModel(PhotoSendModel model) {
        this.id = model.id;
        this.uri = model.uri;
        this.time = model.time;
        this.type = model.type;
        this.text = model.text;
        this.latlng = model.latlng;
    }


    public Uri getUri() {
        return uri;
    }

    public String getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public String getText() {
        return text;
    }

    public String getLatlng() {
        return latlng;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "PhotoSendModel{" +
                "uri=" + uri +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                ", text='" + text + '\'' +
                ", latlng='" + latlng + '\'' +
                '}';
    }




}
