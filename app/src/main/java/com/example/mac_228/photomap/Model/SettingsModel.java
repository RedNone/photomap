package com.example.mac_228.photomap.Model;

public class SettingsModel {

    private int id = 0;
    private String text = "";
    private int color = 0;

    public SettingsModel(int id, String text, int color) {
        this.id = id;
        this.text = text;
        this.color = color;

    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public int getColor() {
        return color;
    }

}
