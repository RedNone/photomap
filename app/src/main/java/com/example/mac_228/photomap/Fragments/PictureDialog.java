package com.example.mac_228.photomap.Fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.Toast;

import com.example.mac_228.photomap.R;

import java.io.IOException;


/**
 * Created by mac-228 on 6/21/17.
 */

public class PictureDialog extends DialogFragment {

    private MapFragment obj;

    private static final String TAG = "PictureDialog";

    public PictureDialog(MapFragment obj) {
        this.obj = obj;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.photo)
                .setItems(R.array.dialog_varibale, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which)
                        {
                            case 0:
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                obj.startActivityForResult(pickPhoto , getTargetRequestCode());
                                break;
                            case 1:
                                Intent takePicture = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                try {
                                    obj.initializeImageUri();
                                } catch (IOException e) {
                                   Log.d(TAG,e.toString());
                                   Toast.makeText(getContext(),getActivity().getString(R.string.error),Toast.LENGTH_SHORT).show();
                                   return;
                                }
                                takePicture.putExtra(MediaStore.EXTRA_OUTPUT, obj.fileUri);
                                obj.startActivityForResult(takePicture, getTargetRequestCode());
                                break;
                        }
                    }
                });
        return builder.create();
    }

}
