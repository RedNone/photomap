package com.example.mac_228.photomap;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.example.mac_228.photomap.Activity.MainActivity;
import com.example.mac_228.photomap.Model.PhotoModel;
import com.example.mac_228.photomap.Model.PhotoSendModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class FireBaseManager implements ValueEventListener {

    public static final String TAG = "FireBaseManager";

    private static FireBaseManager fireBaseManager;

    private FirebaseAuth mAuth;
    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;
    private String userId;
    private List<PhotoSendModel> newDataList;
    private MyTask myTask;
    private Query lastQuery;

    public int dataStatusForMap = 0;

    private Context context;
    public int dataStatusForTimeLine = 0;

    public List<PhotoSendModel> getNewDataList() {
        return newDataList;
    }

    public void setContext(Context context) {
        this.context = context;
    }


    public static FireBaseManager getInstance() {
        if (fireBaseManager == null) {
            fireBaseManager = new FireBaseManager();
        }

        return fireBaseManager;
    }

    public void prepareObjects() {
        newDataList = new ArrayList<>();
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("photos");

        mAuth = FirebaseAuth.getInstance();
    }

    private FireBaseManager() {

    }

    public void uploadData() {

        final List<PhotoModel> list = new ArrayList<>();
        mFirebaseDatabase = mFirebaseInstance.getReference("photos/" + mAuth.getCurrentUser().getUid());
        mFirebaseDatabase.addValueEventListener(this);
    }

    public void updateData(int id, String type, String text) {
        mFirebaseInstance = FirebaseDatabase.getInstance();

        mFirebaseDatabase = mFirebaseInstance.getReference("photos/" + mAuth.getCurrentUser().getUid());

        mFirebaseDatabase.child(String.valueOf(id)).child("type").setValue(type);
        mFirebaseDatabase.child(String.valueOf(id)).child("text").setValue(text);
    }


    public void sendData(final PhotoSendModel model) {

        mFirebaseInstance = FirebaseDatabase.getInstance();

        mFirebaseDatabase = mFirebaseInstance.getReference("photos/" + mAuth.getCurrentUser().getUid());

        lastQuery = mFirebaseDatabase.orderByKey();

        lastQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                int id;
                List<PhotoModel> person = (List<PhotoModel>) dataSnapshot.getValue();

                if (person == null) {
                    id = -1;

                    PhotoModel photo = new PhotoModel
                            (
                                    id + 1,
                                    model.getTime(),
                                    model.getLatlng(),
                                    model.getText(),
                                    encodeImage(model.getUri()),
                                    model.getType()
                            );
                    mFirebaseDatabase.child(String.valueOf(id + 1)).setValue(photo);
                } else {
                    Map<String, Long> map = (Map<String, Long>) person.get(person.size() - 1);
                    id = map.get("id").intValue();

                    PhotoModel photo = new PhotoModel
                            (
                                    id + 1,
                                    model.getTime(),
                                    model.getLatlng(),
                                    model.getText(),
                                    encodeImage(model.getUri()),
                                    model.getType()
                            );


                    mFirebaseDatabase.child(String.valueOf(id + 1)).setValue(photo);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d(TAG, databaseError.toString());
            }
        });

    }


    private String encodeImage(Uri imageUri) {


        InputStream imageStream = null;
        try {
            imageStream = context.getContentResolver().openInputStream(imageUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.JPEG, 70, baos);
        byte[] b = baos.toByteArray();
        String encImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encImage;

    }

    public FirebaseAuth getAuth() {

        return mAuth;
    }


    public void clearAllData() {

        mFirebaseInstance = null;

        if (mFirebaseDatabase != null) {
            mFirebaseDatabase.removeEventListener(this);
            mFirebaseDatabase = null;
        }
        newDataList = null;
        if (myTask != null) {
            myTask.cancel(false);
            myTask = null;
        }
    }

    public void setManager() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("photos");

        mAuth = FirebaseAuth.getInstance();
    }

    public FirebaseUser getCurrentUser() {
        return mAuth.getCurrentUser();
    }

    @Override
    public void onDataChange(DataSnapshot dataSnapshot) {

        final List<PhotoModel> list = new ArrayList<>();

        for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

            PhotoModel model = postSnapshot.getValue(PhotoModel.class);
            list.add(model);
            Log.d(TAG, model.toString());
        }
        if (!list.isEmpty()) {
            myTask = new MyTask(list);
            myTask.execute();
        }
    }

    @Override
    public void onCancelled(DatabaseError databaseError) {}


    class MyTask extends AsyncTask<Void, Void, List<PhotoSendModel>> {
        private List<PhotoModel> list;

        public MyTask(List<PhotoModel> list) {
            this.list = list;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.d(TAG, "Start execute");
        }

        @Override
        protected List<PhotoSendModel> doInBackground(Void... voids) {
            List<PhotoSendModel> newData = new ArrayList<>();
            //String root = "/storage/emulated/0/PhotoMap_Folder/download_images/";
            String root = Environment.getExternalStorageDirectory().toString()
                    + "/PhotoMap_Folder/download_images/" + mAuth.getCurrentUser().getEmail() + "/";
            String imageFolderPath;
            File imagesFolder = new File(root);
            if (!imagesFolder.exists()) {
                imagesFolder.mkdirs();
            }
            for (PhotoModel model : list) {
                if (isCancelled()) {
                 Log.d(TAG,"isCancelld " + isCancelled());
                    return null;
                }
                imageFolderPath = root + model.id + ".png";
                FileOutputStream fos = null;
                try {
                    if (model.photo != null) {
                        fos = new FileOutputStream(new File(imageFolderPath), true);
                        byte[] decodedString = android.util.Base64.decode(model.photo, android.util.Base64.DEFAULT);
                        fos.write(decodedString);
                        fos.flush();
                        fos.close();
                    }

                } catch (Exception e) {
                    Log.d(TAG, e.toString());
                } finally {
                    if (fos != null) {
                        fos = null;
                    }
                }

                newData.add(new PhotoSendModel(
                        model.id,
                        Uri.fromFile(new File(imageFolderPath)),
                        model.date,
                        model.type,
                        model.text,
                        model.latLang
                ));

                Log.d(TAG, new PhotoSendModel(
                        model.id,
                        Uri.fromFile(new File(imageFolderPath)),
                        model.date,
                        model.type,
                        model.text,
                        model.latLang
                ).toString());

            }

            return newData;
        }

        @Override
        protected void onPostExecute(List<PhotoSendModel> photoSendModels) {
            super.onPostExecute(photoSendModels);

            if (photoSendModels != null) {

                newDataList = photoSendModels;

                dataStatusForMap = 1;
                dataStatusForTimeLine = 1;

                Intent intent = new Intent(MainActivity.BROADCAST_ACTION);

                intent.putExtra(MainActivity.PARAM_TASK, MainActivity.STATUS_COME);

                context.sendBroadcast(intent);
            }
        }
    }

}
