package com.example.mac_228.photomap;

import com.example.mac_228.photomap.Model.PhotoSendModel;
import com.example.mac_228.photomap.Model.TimeLineDataModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class TimeLineDataController {

    private List<TimeLineDataModel> mList = null;

    private List<PhotoSendModel> comeList = null;


    public TimeLineDataController(List<PhotoSendModel> list) {

        if (list.isEmpty()) {
            return;
        }

        comeList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            comeList.add(new PhotoSendModel(list.get(i)));
        }

        Collections.sort(comeList, new Comparator<PhotoSendModel>() {
            @Override
            public int compare(PhotoSendModel model, PhotoSendModel t1) {
                Date modelDate = getDate(model);
                Date t1Date = getDate(t1);
                return modelDate.getTime() > t1Date.getTime() ? -1 : modelDate.getTime() == t1Date.getTime() ? 0 : 1;
            }
        });

        prepareData(comeList);
    }

    public void prepareData(List<PhotoSendModel> list) {

        mList = new ArrayList<>();

        for (PhotoSendModel model : list) {

            mList.add(getTimeLineObject(model));
        }

        for (int i = 0; i < mList.size(); i++) {
            if (i == 0) {
                mList.add(i, new TimeLineDataModel(mList.get(i).getData().getIndex()));
                break;
            }
            if (!((i + 1) == mList.size())) {
                if (!mList.get(i).getData().getIndex().equals(mList.get(i + 1).getData().getIndex())) {
                    mList.add(i + 1, new TimeLineDataModel(mList.get(i + 1).getData().getIndex()));
                }
            }
        }
    }

    private TimeLineDataModel getTimeLineObject(PhotoSendModel model) {

        String newTime = "";

        SimpleDateFormat format = new SimpleDateFormat("MMMM dd'th',yyyy - hh:mm a");
        try {
            Date date = format.parse(model.getTime());
            SimpleDateFormat newformat = new SimpleDateFormat("yyyy'.'MM'.'dd");
            newTime = newformat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


        String newText = model.getText().replaceAll("#([A-Za-z0-9_-]+)", "");


        return new TimeLineDataModel(model.getId(),
                model.getUri(),
                newTime,
                newText,
                model.getType(),
                getTimeString(model));
    }

    private String getTimeString(PhotoSendModel model) {

        String newTime = null;
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd'th',yyyy - hh:mm a");
        try {
            Date date = format.parse(model.getTime());
            SimpleDateFormat newformat = new SimpleDateFormat("MMMM yyyy");
            newTime = newformat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return newTime;

    }

    public List<PhotoSendModel> getComeList() {
        return comeList;
    }

    public List<TimeLineDataModel> getList() {
        return mList;
    }

    private Date getDate(PhotoSendModel model) {

        Date date = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("MMMM dd'th',yyyy - hh:mm a");
            date = format.parse(model.getTime());

        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public void prepareHashTagsList(String newText) {
        List<PhotoSendModel> mHashTagsList = new ArrayList<>();

        for (PhotoSendModel model : comeList) {
            if (model.getText().contains(newText)) {
                mHashTagsList.add(model);
            }
        }
        if (!mHashTagsList.isEmpty()) {
            prepareData(mHashTagsList);
        }
    }
}
