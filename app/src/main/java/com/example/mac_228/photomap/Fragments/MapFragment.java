package com.example.mac_228.photomap.Fragments;


import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mac_228.photomap.Activity.MainActivity;
import com.example.mac_228.photomap.Activity.PhotoDetailsActivity;
import com.example.mac_228.photomap.Activity.SettingsActivity;
import com.example.mac_228.photomap.Adapter.SettingAdapter;
import com.example.mac_228.photomap.BuildConfig;
import com.example.mac_228.photomap.FireBaseManager;
import com.example.mac_228.photomap.Model.PhotoSendModel;
import com.example.mac_228.photomap.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;
import static android.content.Context.MODE_PRIVATE;


public class MapFragment extends BaseFragment implements OnMapReadyCallback,
        LocationListener,
        View.OnClickListener,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnInfoWindowClickListener {

    public static final String TAG = "MapFragment";

    private GoogleMap mMap;
    private MapView mapView;

    private LocationManager locationManager;
    private String provider;

    private RelativeLayout mLayout;

    private FloatingActionButton mCameraButton, mMode;

    private Location location;

    private LatLng locationImage;

    private int NAVIGATION_MODE = 0;
    private int FIRST_UPDATE = 0;

    private final int LONG_PRESS_MAP = 0;
    private final int BUTTON_PRESS = 1;
    private final int EXISTING_PHOTO = 2;

    private final int GALLERY_TYPE = 0;
    private final int CAMERA_TYPE = 1;


    private final int GPS_IS_ON = 2;
    private String root;
    private String imageFolderPath;
    private String imageName;
    public Uri fileUri;

    private SharedPreferences sPreferences;

    private FireBaseManager mManager;

    private BroadcastReceiver br;

    private List<PhotoSendModel> newData;

    private List<Marker> markerList;


    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        markerList = new ArrayList<>();

        mManager = FireBaseManager.getInstance();

        sPreferences = getActivity().getSharedPreferences(SettingAdapter.APP_PREFERENCES, MODE_PRIVATE);

        mLayout = (RelativeLayout) view.findViewById(R.id.mapRelative);

        mCameraButton = (FloatingActionButton) view.findViewById(R.id.floatingActionMapCamera);
        mMode = (FloatingActionButton) view.findViewById(R.id.floatingActionMapMode);

        mCameraButton.setOnClickListener(this);
        mMode.setOnClickListener(this);

        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        MapsInitializer.initialize(getActivity());


        br = new BroadcastReceiver() {
            // действия при получении сообщений
            public void onReceive(Context context, Intent intent) {

                boolean status = intent.getBooleanExtra(MainActivity.PARAM_TASK, false);

                if (!FireBaseManager.getInstance().getNewDataList().isEmpty()) {
                    prepareNewData();
                }
            }
        };

        setHasOptionsMenu(true);

        return view;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem action_logout = menu.findItem(R.id.action_logOut);
        MenuItem action_settings = menu.findItem(R.id.action_settings);
        MenuItem action_search = menu.findItem(R.id.action_search);

        Log.d(TAG, "onPrepareOptionsMenu");


        action_logout.setVisible(true);
        action_settings.setVisible(true);
        action_search.setVisible(false);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }


    private void prepareNewData() {
        if (mManager.getNewDataList().isEmpty()) {
            return;
        }

        boolean friends = sPreferences.getBoolean(getString(R.string.friends), true);

        boolean nature = sPreferences.getBoolean(getString(R.string.nature), true);

        boolean default_type = sPreferences.getBoolean(getString(R.string.default_type), true);

        boolean food = sPreferences.getBoolean(getString(R.string.food),true);


        newData = new ArrayList<>();

        for (PhotoSendModel obj : mManager.getNewDataList()) {
            newData.add(new PhotoSendModel(obj));
        }

        for (int i = 0; i < newData.size(); i++) {
            PhotoSendModel model = newData.get(i);
            if (model.getType().equals(getString(R.string.friends))) {
                if (friends != true) {
                    newData.remove(i);
                    i--;
                }
            }
            if (model.getType().equals(getString(R.string.nature))) {
                if (nature != true) {
                    newData.remove(i);
                    i--;
                }
            }
            if (model.getType().equals(getString(R.string.default_type))) {
                if (default_type != true) {
                    newData.remove(i);
                    i--;
                }
            }
            if (model.getType().equals(getString(R.string.food))) {
                if (food != true) {
                    newData.remove(i);
                    i--;
                }
            }
        }

        for (int i = 0; i < newData.size(); i++) {
            newData.get(i).setTime(formatTime(newData.get(i).getTime()));
        }


        addMarkers(newData);
        mManager.dataStatusForMap = 0;

    }

    private void addMarkers(List<PhotoSendModel> newData) {

        if (!markerList.isEmpty()) {
            for (int i = 0; i < markerList.size(); i++) {
                markerList.get(i).remove();
            }

        }
        markerList.clear();
        for (int i = 0; i < newData.size(); i++) {
            Log.d(TAG, newData.get(i).getUri().toString());

            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(getLatLng(newData.get(i).getLatlng())));

            if (newData.get(i).getType().equals(getString(R.string.friends))) {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            if (newData.get(i).getType().equals(getString(R.string.nature))) {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            }
            if (newData.get(i).getType().equals(getString(R.string.default_type))) {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
            }
            if (newData.get(i).getType().equals(getString(R.string.food))) {
                marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
            }

            marker.setTitle(String.valueOf(newData.get(i).getId()));
            markerList.add(marker);

        }


    }

    private LatLng getLatLng(String str) {
        int first = str.indexOf("(");
        int second = str.indexOf(",");
        int last = str.indexOf(")");

        double lat = Double.parseDouble(str.substring(first + 1, second));
        double lng = Double.parseDouble(str.substring(second + 1, last));
        return new LatLng(lat, lng);
    }

    private String formatTime(String time) {

        String newTime = null;
        SimpleDateFormat format = new SimpleDateFormat("MMMM dd'th',yyyy - hh:mm a");
        try {
            Date date = format.parse(time);
            SimpleDateFormat newformat = new SimpleDateFormat("yyyy'.'MM'.'dd");
            newTime = newformat.format(date);
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }
        return newTime;
    }


    public void initializeImageUri() throws IOException {

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera");
        File image = null;

            image = File.createTempFile(
                    imageFileName,
                    ".jpg",
                    storageDir
            );



        imageFolderPath = "file:" + image.getAbsolutePath();
        fileUri = FileProvider.getUriForFile(getActivity(),
                BuildConfig.APPLICATION_ID + ".provider",
                image);

    }


    private void initializeLocationManager() {

        locationManager = (LocationManager) getActivity().getSystemService(getContext().LOCATION_SERVICE);
        provider = locationManager.getBestProvider(new Criteria(), false);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(provider, 1000, 0, MapFragment.this);
    }


    private Location getLastKnownLocation() {
        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        provider = locationManager.getBestProvider(new Criteria(), false);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        for (String provider : providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null) {
                continue;
            }
            if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = l;
            }
        }

        return bestLocation;
    }

    @Override
    public void onResume() {
        initializeLocationManager();


        if (mManager.getNewDataList().isEmpty()) {
            Snackbar.make(mLayout, R.string.data_loadind, Snackbar.LENGTH_LONG).show();
        }

        mapView.onResume();
        super.onResume();
        IntentFilter intFilt = new IntentFilter(MainActivity.BROADCAST_ACTION);
        getActivity().registerReceiver(br, intFilt);
        if (mManager.dataStatusForMap == 1) {
            prepareNewData();
        } else {
            if (SettingsActivity.SETTINGS_UPDATE == true) {
                prepareNewData();
                SettingsActivity.SETTINGS_UPDATE = false;
            }
        }

    }

    @Override
    public void onPause() {
        FIRST_UPDATE = 0;
        locationManager.removeUpdates(this);
        super.onPause();
        mapView.onPause();
        getActivity().unregisterReceiver(br);
    }

    private void checkGPS() {
        boolean statusOfGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        Log.d(TAG, String.valueOf(statusOfGPS));
        if (!statusOfGPS) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle(R.string.gps_off)
                    .setMessage(R.string.gps_message)
                    .setNegativeButton(R.string.gps_negative_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.cancel();
                        }
                    })
                    .setPositiveButton(R.string.gps_positive_button, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            MapFragment.this.startActivityForResult(intent, GPS_IS_ON);
                        }
                    }).create().show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapLongClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);


        location = getLastKnownLocation();

        if (location != null) {
            Log.d(TAG, "LOCATION NOT NULL");
            onLocationChanged(location);
        } else {
            Snackbar.make(mLayout, R.string.locationIsNull, Snackbar.LENGTH_SHORT).show();
        }

        mMap.setInfoWindowAdapter(new MyInfoWindowAdapter());
    }

    @Override
    public void onLocationChanged(Location location) {


        if (FIRST_UPDATE == 0) {
            FIRST_UPDATE = 1;

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        if (NAVIGATION_MODE == 1) {

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))      // Sets the center of the map to location user
                    .zoom(17)                   // Sets the zoom
                    .tilt(40)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }


    @Override
    public void onProviderEnabled(String s) {

        location = getLastKnownLocation();

        if (location != null) {
            Log.d(TAG, "LOCATION NOT NULL");
            onLocationChanged(location);
        } else {
            Log.d(TAG, "LOCATION IS NULL PROVIDER");
        }
    }

    @Override
    public void onProviderDisabled(String s) {
        Snackbar.make(mLayout, "Provider is disabled", Snackbar.LENGTH_SHORT).show();
        checkGPS();
        FIRST_UPDATE = 0;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatingActionMapMode:
                changeMode();
                break;
            case R.id.floatingActionMapCamera:
                openPictureDialog(BUTTON_PRESS);
                break;
        }
    }

    private void changeMode() {

        checkGPS();
        location = getLastKnownLocation();

        if (location == null) {
            Snackbar.make(mLayout, R.string.locationIsNull, Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (NAVIGATION_MODE == 0) {
            NAVIGATION_MODE = 1;
            mMap.getUiSettings().setScrollGesturesEnabled(false);
            mMode.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorFabEnabled)));
            onLocationChanged(location);
        } else {
            NAVIGATION_MODE = 0;
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMode.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorFabDisabled)));
        }
    }

    private void openPictureDialog(int code) {
        PictureDialog pictureDialog = new PictureDialog(this);

        if (code == BUTTON_PRESS) {
            pictureDialog.setTargetFragment(this, BUTTON_PRESS);
            pictureDialog.show(getFragmentManager(), pictureDialog.getClass().getName());
        }
        if (code == LONG_PRESS_MAP) {
            pictureDialog.setTargetFragment(this, LONG_PRESS_MAP);
            pictureDialog.show(getFragmentManager(), pictureDialog.getClass().getName());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResultMAP");

        switch (requestCode) {
            case BUTTON_PRESS:
                if (data != null) {
                    if (data.getData() != null) {
                        if (!checkLocation() && checkNetwork()) {

                            return;
                        }
                        startActivity(prepareImageIntent(data.getData().toString(), BUTTON_PRESS, GALLERY_TYPE));
                    }
                } else {
                    if (fileUri != null) {
                        if (!checkLocation() && checkNetwork()) {
                            fileUri = null;
                            return;
                        }
                        startActivity(prepareImageIntent(fileUri.toString(), BUTTON_PRESS, CAMERA_TYPE));
                        fileUri = null;
                    }

                }
                break;
            case LONG_PRESS_MAP:
                if (data != null) {
                    if (data.getData() != null) {
                        if (!checkNetwork()) {
                            return;
                        }
                        startActivity(prepareImageIntent(data.getData().toString(), LONG_PRESS_MAP, GALLERY_TYPE));
                    }
                } else {
                    if (fileUri != null) {
                        if (!checkNetwork()) {
                            fileUri = null;
                            return;
                        }
                        startActivity(prepareImageIntent(fileUri.toString(), LONG_PRESS_MAP, CAMERA_TYPE));
                        fileUri = null;
                    }
                }
                break;
            case GPS_IS_ON:
                location = getLastKnownLocation();
                if (location != null) {
                    onLocationChanged(location);
                }
                break;
        }

    }


    private boolean checkLocation() {
        if (location == null) {
            Snackbar.make(mLayout, R.string.locationIsNull, Snackbar.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }
    }

    public boolean checkNetwork() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            Snackbar.make(mLayout, R.string.network_disable, Snackbar.LENGTH_SHORT).show();
            return false;
        }
    }

    private String getLatLng() {
        String str = "lat/lng:";
        str += "(" + location.getLatitude() + "," + location.getLongitude() + ")";
        return str;
    }


    private Intent prepareImageIntent(String data, int flag, int type) {
        Intent intent = new Intent(getActivity(), PhotoDetailsActivity.class);

        if (flag == BUTTON_PRESS) {
            if (type == GALLERY_TYPE) {
                intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.NEW_IMAGE_BUTTON);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_URI, data);
                intent.putExtra(PhotoDetailsActivity.TYPE_OF_IMAGE, GALLERY_TYPE);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_LOCATION, getLatLng());

            }
            if (type == CAMERA_TYPE) {
                intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.NEW_IMAGE_BUTTON);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_URI, data);
                intent.putExtra(PhotoDetailsActivity.TYPE_OF_IMAGE, CAMERA_TYPE);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_LOCATION, getLatLng());
            }
        }
        if (flag == LONG_PRESS_MAP) {
            if (type == GALLERY_TYPE) {
                intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.NEW_IMAGE_LONGPRESS);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_URI, data);
                intent.putExtra(PhotoDetailsActivity.TYPE_OF_IMAGE, GALLERY_TYPE);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_LOCATION, locationImage.toString());
            }
            if (type == CAMERA_TYPE) {
                intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.NEW_IMAGE_LONGPRESS);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_URI, data);
                intent.putExtra(PhotoDetailsActivity.TYPE_OF_IMAGE, CAMERA_TYPE);
                intent.putExtra(PhotoDetailsActivity.NEW_IMAGE_LOCATION, locationImage.toString());
            }

        }
        if (flag == EXISTING_PHOTO) {

            intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.EXISTING_PHOTO);
            intent.putExtra(PhotoDetailsActivity.EXISTING_PHOTO_ID, data);

        }
        return intent;
    }

    @Override
    public void onMapLongClick(LatLng latLng) {


        openPictureDialog(LONG_PRESS_MAP);
        locationImage = latLng;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        startActivity(prepareImageIntent(marker.getTitle().toString(), EXISTING_PHOTO, 0));
    }

    class MyInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        MyInfoWindowAdapter() {
            myContentsView = getActivity().getLayoutInflater().inflate(R.layout.marker_layout, null);
        }

        @Override
        public View getInfoContents(Marker marker) {
            PhotoSendModel obj = null;
            for (PhotoSendModel model : newData) {
                if (model.getId() == Integer.valueOf(marker.getTitle())) {
                    obj = model;
                    break;
                }
            }

            TextView tvTitle = ((TextView) myContentsView.findViewById(R.id.textViewMarkerData));
            tvTitle.setText(obj.getText());
            TextView tvSnippet = ((TextView) myContentsView.findViewById(R.id.textViewMarkerDate));
            tvSnippet.setText(obj.getTime());

            ImageView imageView = ((ImageView) myContentsView.findViewById(R.id.imageViewMarker));
            imageView.setImageURI(obj.getUri());


            return myContentsView;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub
            return null;
        }

    }


}
