package com.example.mac_228.photomap.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mac_228.photomap.Activity.PhotoDetailsActivity;
import com.example.mac_228.photomap.Model.TimeLineDataModel;
import com.example.mac_228.photomap.R;

import java.util.List;

public class TimeLineAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<TimeLineDataModel> mList;

    private final int SECTION_TYPE = 1;
    private final int SECTION_DATA = 2;

    private Context mContext;


    public TimeLineAdapter(List<TimeLineDataModel> mList, Context mContext) {
        this.mList = mList;
        this.mContext = mContext;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;

        switch (viewType) {
            case SECTION_TYPE:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_line_recycler_sections, parent, false);
                return new SectionViewHolder(view);
            case SECTION_DATA:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.time_line_recycler_data, parent, false);
                return new DataViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final TimeLineDataModel object = mList.get(position);
        if (object != null) {
            if (object.getSectionName() == null) {

                ((DataViewHolder) holder).mTitle.setText(object.getData().getText());
                ((DataViewHolder) holder).mDate.setText(object.getData().getTime());
                ((DataViewHolder) holder).mType.setText(object.getData().getType());
                ((DataViewHolder) holder).mImage.setImageURI(object.getData().getUri());
                ((DataViewHolder) holder).mLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(mContext, PhotoDetailsActivity.class);
                        intent.putExtra(PhotoDetailsActivity.TAG, PhotoDetailsActivity.EXISTING_PHOTO);
                        intent.putExtra(PhotoDetailsActivity.EXISTING_PHOTO_ID, String.valueOf(object.getData().getId()));
                        mContext.startActivity(intent);
                    }
                });

            } else {
                ((SectionViewHolder) holder).mTitle.setText(object.getSectionName());
            }

        }
    }

    @Override
    public int getItemCount() {
        if (mList == null)
            return 0;
        return mList.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mList != null) {

            TimeLineDataModel object = mList.get(position);

            if (object.getSectionName() == null) {
                return SECTION_DATA;
            } else {
                return SECTION_TYPE;
            }

        }
        return -1;
    }

    public static class SectionViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;

        public SectionViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.textViewTimeLineSection);
        }
    }

    public static class DataViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitle;
        private TextView mDate;
        private TextView mType;
        private ImageView mImage;
        private LinearLayout mLayout;

        public DataViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.textViewTextTimeLine);
            mDate = (TextView) itemView.findViewById(R.id.textViewDateTimeLine);
            mType = (TextView) itemView.findViewById(R.id.textViewTypeTimeLine);
            mImage = (ImageView) itemView.findViewById(R.id.imageViewTimeLine);
            mLayout = (LinearLayout) itemView.findViewById(R.id.timeLineLayout);
        }
    }
}