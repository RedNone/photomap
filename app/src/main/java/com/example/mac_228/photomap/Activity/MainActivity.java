package com.example.mac_228.photomap.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.mac_228.photomap.FireBaseManager;
import com.example.mac_228.photomap.Fragments.LoginFragment;
import com.example.mac_228.photomap.Fragments.MainFragment;
import com.example.mac_228.photomap.R;
import com.google.firebase.FirebaseApp;

public class MainActivity extends AppCompatActivity {

    public static final String PARAM_TASK = "GETDATA";
    public static final boolean STATUS_COME = true;
    private static final int REQUEST_PERMISSON = 101;

    private final String TAG = "MainActivity";

    public FragmentManager fragmentManager;
    private FireBaseManager mManager;

    private Toolbar toolbar;

    public static final int LOGIN_FRAGMENT = 0;
    public static final int MAIN_FRAGMENT = 1;


    public final static String BROADCAST_ACTION = "com.example.mac_228.photomap";

    public SearchView mSearchView;
    private MenuItem searchMenuItem;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragmentManager = getSupportFragmentManager();

        initializeFireBase();

        if (!checkUser()) {
            changeFragment(LOGIN_FRAGMENT);
        } else {
            if (checkPermisson()) {
                changeFragment(MAIN_FRAGMENT);
            }
        }

    }

    private void initializeFireBase() {

        FirebaseApp.initializeApp(this);
        mManager = FireBaseManager.getInstance();
        mManager.setContext(this);
        mManager.prepareObjects();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logOut) {
            mManager.getAuth().signOut();
            changeFragment(LOGIN_FRAGMENT);
            return true;
        }
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public boolean checkUser() {
        if (FireBaseManager.getInstance().getCurrentUser() == null) {
            return false;
        }
        return true;


    }

    private boolean checkPermisson() {

        boolean writePermission = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        boolean locationPermission = (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
        boolean cameraPermisson = (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);

        if (!writePermission | !locationPermission | !cameraPermisson) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.CAMERA},
                    REQUEST_PERMISSON);
        }


        return writePermission && locationPermission && cameraPermisson;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSON: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    changeFragment(MAIN_FRAGMENT);
                } else {
                    Toast.makeText(this, getString(R.string.permisson_exeption), Toast.LENGTH_LONG).show();

                }
            }
        }
    }


    public void changeFragment(int flag) {
        Log.d(TAG, "changeFragment" + " " + flag);

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (flag == LOGIN_FRAGMENT) {
            if (fragmentManager.findFragmentByTag(LoginFragment.TAG) == null) {
                fragmentTransaction.replace(R.id.conteiner, new LoginFragment(), LoginFragment.TAG);
            }
            mManager.clearAllData();

        }
        if (flag == MAIN_FRAGMENT) {
            if (fragmentManager.findFragmentByTag(MainFragment.TAG) == null) {
                fragmentTransaction.replace(R.id.conteiner, new MainFragment(), MainFragment.TAG);
            }
            initializeFireBase();
            mManager.setManager();
            mManager.uploadData();

        }
        fragmentTransaction.commit();

    }

}
