package com.example.mac_228.photomap.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mac_228.photomap.Activity.SettingsActivity;
import com.example.mac_228.photomap.Model.SettingsModel;
import com.example.mac_228.photomap.R;

import java.util.List;


public class SettingAdapter extends RecyclerView.Adapter<SettingAdapter.ViewHolder> {

    List<SettingsModel> list = null;

    public static final String APP_PREFERENCES = "PhotoMapSettings";
    private SharedPreferences mSettings = null;
    private SharedPreferences.Editor editor = null;
    private Context context = null;

    public SettingAdapter(List<SettingsModel> list, Context context) {
        this.list = list;
        this.context = context;
        mSettings = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        editor = mSettings.edit();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.settings_content, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final SettingsModel obj = list.get(position);

        holder.mText.setText(obj.getText());
        holder.mText.setTextColor(context.getResources().getColor(obj.getColor()));


        if (obj.getText().equals(context.getString(R.string.default_type))) {
            holder.mBox.setButtonDrawable(context.getResources().getDrawable(R.drawable.xml_checkbutton_defualt));
        }
        if (obj.getText().equals(context.getString(R.string.nature))) {
            holder.mBox.setButtonDrawable(context.getResources().getDrawable(R.drawable.xml_checkbutton_nature));
        }
        if (obj.getText().equals(context.getString(R.string.friends))) {
            holder.mBox.setButtonDrawable(context.getResources().getDrawable(R.drawable.xml_checkbutton_friends));
        }
        if(obj.getText().equals(context.getString(R.string.food))){
            holder.mBox.setButtonDrawable(context.getResources().getDrawable(R.drawable.xml_checkbutton_food));
        }


        if (mSettings.contains(obj.getText())) {
            holder.mBox.setChecked(mSettings.getBoolean(obj.getText(), true));
        }

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettings.contains(obj.getText())) {
                    holder.mBox.setChecked(!mSettings.getBoolean(obj.getText(), true));
                } else {
                    holder.mBox.setChecked(false);
                }

            }
        });


        holder.mBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                editor.putBoolean(obj.getText(), b);
                editor.apply();
                SettingsActivity.SETTINGS_UPDATE = true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView mText;
        private CheckBox mBox;
        private LinearLayout mLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            mText = (TextView) itemView.findViewById(R.id.settingsTextView);
            mBox = (CheckBox) itemView.findViewById(R.id.settingsCheckBox);
            mLayout = (LinearLayout) itemView.findViewById(R.id.settingLiner);
        }
    }
}
