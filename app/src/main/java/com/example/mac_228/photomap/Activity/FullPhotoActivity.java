package com.example.mac_228.photomap.Activity;

import android.animation.Animator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.mac_228.photomap.R;
import com.github.chrisbanes.photoview.PhotoView;

public class FullPhotoActivity extends AppCompatActivity implements View.OnClickListener {

    private PhotoView mImage;
    private Toolbar toolbar;

    private boolean mIsPhotoDetailsVisible = true;

    private LinearLayout mLayout;

    private TextView mDate, mDiscription;

    public static final String DATE_EXTRA = "dateExtra";
    public static final String DISCRIPTION_EXTRA = "discriptionExtra";
    public static final String IMAGE_URI = "imageURI";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_photo);
        toolbar = (Toolbar) findViewById(R.id.toolbarFullPhoto);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(null);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_arrow_left));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mImage = (PhotoView) findViewById(R.id.imageViewPhoto);
        mImage.setOnClickListener(this);


        mLayout = (LinearLayout) findViewById(R.id.layoutDescription);

        mDiscription = (TextView) findViewById(R.id.textViewPhotoDescription);
        mDate = (TextView) findViewById(R.id.textViewPhotoDate);

        getData();
    }

    private void getData() {
        Intent intent = getIntent();
        mDate.setText(intent.getStringExtra(DATE_EXTRA));

        String str = intent.getStringExtra(DISCRIPTION_EXTRA);

        if (!str.equals("")) {
            mDiscription.setText(str);
        } else {
            mDiscription.setText(null);
        }


        mImage.setImageURI(Uri.parse(intent.getStringExtra(IMAGE_URI)));
    }

    private void hideDetails() {
        if (mIsPhotoDetailsVisible) {
            mIsPhotoDetailsVisible = false;

            toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
                @Override
                public void onAnimationEnd(Animator animator) {
                    toolbar.setVisibility(View.INVISIBLE);
                }
            }).start();

            mLayout.animate().translationY(mLayout.getBottom()).setInterpolator(new AccelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {}

                @Override
                public void onAnimationEnd(Animator animator) {
                    mLayout.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            }).start();
        } else {
            mIsPhotoDetailsVisible = true;

            toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    toolbar.setVisibility(View.VISIBLE);
                }
                @Override
                public void onAnimationEnd(Animator animator) {}
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            }).start();


            mLayout.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {
                    mLayout.setVisibility(View.VISIBLE);
                }
                @Override
                public void onAnimationEnd(Animator animator) {}
                @Override
                public void onAnimationCancel(Animator animator) {}
                @Override
                public void onAnimationRepeat(Animator animator) {}
            }).start();

        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.imageViewPhoto:
                hideDetails();
                break;
            case R.id.toolbarFullPhoto:
                finish();
                break;
        }
    }
}
