package com.example.mac_228.photomap.Model;

import android.net.Uri;


public class TimeLineDataModel {


    private ListData mData = null;
    private String mSectionName = null;

    public TimeLineDataModel(int id, Uri uri, String time, String text, String type, String index) {
        mData = new ListData(id, uri, time, text, type, index);
    }

    public TimeLineDataModel(String sectionName) {
        mSectionName = sectionName;
    }

    public ListData getData() {
        return mData;
    }


    public String getSectionName() {
        return mSectionName;
    }

    public class ListData {
        private int mId = 0;
        private Uri mUri = null;
        private String mTime = "";
        private String mText = "";
        private String mType = "";
        private String mIndex = "";

        public ListData(int id, Uri uri, String time, String text, String type, String index) {
            mId = id;
            mUri = uri;
            mTime = time;
            mText = text;
            mType = type;
            mIndex = index;
        }

        public int getId() {
            return mId;
        }

        public Uri getUri() {
            return mUri;
        }

        public String getTime() {
            return mTime;
        }

        public String getText() {
            return mText;
        }

        public String getType() {
            return mType;
        }

        public String getIndex() {
            return mIndex;
        }

        @Override
        public String toString() {
            return "ListData{" +
                    "mId=" + mId +
                    ", mUri=" + mUri +
                    ", mTime='" + mTime + '\'' +
                    ", mText='" + mText + '\'' +
                    ", mType='" + mType + '\'' +
                    ", mIndex='" + mIndex + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "TimeLineDataModel{" +
                "mData=" + mData.toString() +
                ", mSectionName='" + mSectionName + '\'' +
                '}';
    }
}
