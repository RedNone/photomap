package com.example.mac_228.photomap.Fragments;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.mac_228.photomap.Activity.MainActivity;
import com.example.mac_228.photomap.Adapter.TimeLineAdapter;
import com.example.mac_228.photomap.FireBaseManager;
import com.example.mac_228.photomap.Model.TimeLineDataModel;
import com.example.mac_228.photomap.R;
import com.example.mac_228.photomap.TimeLineDataController;

import java.util.List;


public class TimeLineFragment extends BaseFragment implements SearchView.OnQueryTextListener {

    public static final String TAG = "TimeLineFragment";

    private BroadcastReceiver mReceiver;
    private RecyclerView mRecycler;
    private ProgressBar mProgress;
    private FireBaseManager mManager;
    private TimeLineDataController mController;

    public SearchView mSearchView;
    private MenuItem searchMenuItem;

    public TimeLineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_time_line, container, false);


        mRecycler = (RecyclerView) view.findViewById(R.id.timeLineRecycler);
        mProgress = (ProgressBar) view.findViewById(R.id.progressBarTimeLine);

        mManager = FireBaseManager.getInstance();

        mReceiver = new BroadcastReceiver() {
            // действия при получении сообщений
            public void onReceive(Context context, Intent intent) {

                boolean status = intent.getBooleanExtra(MainActivity.PARAM_TASK, false);

                if (!FireBaseManager.getInstance().getNewDataList().isEmpty()) {
                    mController = new TimeLineDataController(mManager.getNewDataList());
                    prepareRecyclerView();
                }
            }
        };

        setHasOptionsMenu(true);
        return view;
    }


    private void prepareRecyclerView() {

        mProgress.setVisibility(View.GONE);

        mRecycler.setVisibility(View.VISIBLE);

        setAdapter(mController.getList());

        mManager.dataStatusForTimeLine = 0;

    }

    private void setAdapter(List<TimeLineDataModel> model) {

        TimeLineAdapter adapter = new TimeLineAdapter(model, getActivity());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), OrientationHelper.VERTICAL, false);

        mRecycler.setLayoutManager(linearLayoutManager);
        mRecycler.setItemAnimator(new DefaultItemAnimator());
        mRecycler.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        IntentFilter intFilt = new IntentFilter(MainActivity.BROADCAST_ACTION);
        getActivity().registerReceiver(mReceiver, intFilt);

        if (mManager.getNewDataList().isEmpty()) {
            mRecycler.setVisibility(View.GONE);
        }

        if (mManager.dataStatusForTimeLine == 1) {
            mProgress.setVisibility(View.GONE);
            mController = new TimeLineDataController(mManager.getNewDataList());
            prepareRecyclerView();
        }

    }

    @Override
    public void onPause() {
        super.onPause();
        getActivity().unregisterReceiver(mReceiver);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(TAG, newText);
        if (!mManager.getNewDataList().isEmpty()) {
            mController.prepareHashTagsList(newText);
            setAdapter(mController.getList());
        }
        return false;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem action_logout = menu.findItem(R.id.action_logOut);
        MenuItem action_settings = menu.findItem(R.id.action_settings);
        MenuItem action_search = menu.findItem(R.id.action_search);


        action_settings.setVisible(true);
        action_search.setVisible(true);
        action_logout.setVisible(true);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        searchMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) searchMenuItem.getActionView();
        mSearchView.setOnQueryTextListener(this);
    }


}
