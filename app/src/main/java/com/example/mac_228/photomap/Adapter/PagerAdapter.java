package com.example.mac_228.photomap.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.mac_228.photomap.Fragments.MapFragment;
import com.example.mac_228.photomap.Fragments.TimeLineFragment;


public class PagerAdapter extends FragmentStatePagerAdapter {


    int tabCount;

    private MapFragment mMap;
    private TimeLineFragment mTimeLine;

    public PagerAdapter(FragmentManager fm,int tabCount) {
        super(fm);
        this.tabCount = tabCount;
        mMap = new MapFragment();
        mTimeLine = new TimeLineFragment();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return mMap;
            case 1:
                return mTimeLine;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }


}
