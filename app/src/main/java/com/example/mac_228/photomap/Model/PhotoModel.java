package com.example.mac_228.photomap.Model;


import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class PhotoModel {

    public int id;
    public String date;
    public String latLang;
    public String text;
    public String photo;
    public String type;

    public PhotoModel() {
    }

    public PhotoModel(int id, String date, String latLang, String text, String photo, String type) {
        this.id = id;
        this.date = date;
        this.latLang = latLang;
        this.text = text;
        this.photo = photo;
        this.type = type;
    }

    @Override
    public String toString() {
        return "PhotoModel{" +
                "id=" + id +
                ", date='" + date + '\'' +
                ", latLang='" + latLang + '\'' +
                ", text='" + text + '\'' +
                ", photo='" + photo + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
