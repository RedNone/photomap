package com.example.mac_228.photomap.Fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;

import com.example.mac_228.photomap.R;

/**
 * Created by mac-228 on 6/16/17.
 */

public class BaseFragment extends Fragment {

    public ProgressDialog mProgressDialog;

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(getContext());
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
