package com.example.mac_228.photomap.Fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.mac_228.photomap.Activity.MainActivity;
import com.example.mac_228.photomap.FireBaseManager;
import com.example.mac_228.photomap.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;


public class LoginFragment extends BaseFragment implements View.OnClickListener {


    public static final String TAG = "LoginFragment";

    public LoginFragment() {
        // Required empty public constructor
    }

    private EditText mEmail, mPassword;
    private Button mSignIn, mCreateAcc;
    private FireBaseManager mManager;
    private CoordinatorLayout mCoordinator;

    private final int LOG_IN_TAG = 1;
    private final int CREATE_ACC_TAG = 2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        setHasOptionsMenu(true);

        mEmail = (EditText) view.findViewById(R.id.emailEditText);
        mPassword = (EditText) view.findViewById(R.id.passwordEditText);

        mSignIn = (Button) view.findViewById(R.id.signInButton);
        mCreateAcc = (Button) view.findViewById(R.id.createLoginButton);

        mCoordinator = (CoordinatorLayout) view.findViewById(R.id.loginCoordinator);

        mSignIn.setOnClickListener(this);
        mCreateAcc.setOnClickListener(this);

        mManager = FireBaseManager.getInstance();

        return view;
    }


    private void logIn(String email, String password, int flag) {
        if (!validateForm()) {
            Snackbar.make(mCoordinator, R.string.incorrectInput, Snackbar.LENGTH_SHORT).show();
            return;
        }

        showProgressDialog();

        if (flag == LOG_IN_TAG) {
            mManager.getAuth().signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "signInWithEmail:success");

                                ((MainActivity) getActivity()).changeFragment(MainActivity.MAIN_FRAGMENT);

                            } else {

                                Log.d(TAG, "signInWithEmail:failure", task.getException());
                                Snackbar.make(mCoordinator, R.string.incorrectInput, Snackbar.LENGTH_SHORT).show();

                            }

                            hideProgressDialog();

                        }
                    });
        }
        if (flag == CREATE_ACC_TAG) {
            mManager.getAuth().createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                ((MainActivity) getActivity()).changeFragment(MainActivity.MAIN_FRAGMENT);

                            } else {
                                // If sign in fails, display a message to the user.
                                Log.d(TAG, "createUserWithEmail:failure", task.getException());
                                Snackbar.make(mCoordinator, R.string.incorrectInput, Snackbar.LENGTH_SHORT).show();

                            }

                            hideProgressDialog();

                        }
                    });
        }


    }

    private boolean validateForm() {
        boolean valid = true;

        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();

        if (TextUtils.isEmpty(email)) {
            mEmail.setError("Required.");
            valid = false;
        } else {
            mEmail.setError(null);
        }


        if (TextUtils.isEmpty(password)) {
            mPassword.setError("Required.");
            valid = false;
        } else {
            mPassword.setError(null);
        }

        return valid;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.signInButton:
                logIn(mEmail.getText().toString(), mPassword.getText().toString(), LOG_IN_TAG);
                break;
            case R.id.createLoginButton:
                logIn(mEmail.getText().toString(), mPassword.getText().toString(), CREATE_ACC_TAG);
                break;
            default:
                return;
        }

    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        MenuItem action_logout = menu.findItem(R.id.action_logOut);
        MenuItem action_settings = menu.findItem(R.id.action_settings);
        MenuItem action_search = menu.findItem(R.id.action_search);

        action_settings.setVisible(false);
        action_search.setVisible(false);
        action_logout.setVisible(false);


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);


    }
}
