package com.example.mac_228.photomap.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.mac_228.photomap.Model.SettingsModel;
import com.example.mac_228.photomap.R;
import com.example.mac_228.photomap.Adapter.SettingAdapter;

import java.util.ArrayList;
import java.util.List;

public class SettingsActivity extends AppCompatActivity {

    private final String TAG = "SettingsActivity";

    private RecyclerView mRecycler = null;

    private List<SettingsModel> list = null;

    public static boolean SETTINGS_UPDATE = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarSettings);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(null);
        toolbar.setNavigationIcon(getResources().getDrawable(R.mipmap.ic_arrow_left));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mRecycler = (RecyclerView) findViewById(R.id.settingsRecycler);

        LinearLayoutManager mManager = new LinearLayoutManager(this);
        mRecycler.setLayoutManager(mManager);

        initializeModel();

        SettingAdapter adapter = new SettingAdapter(list,this);
        mRecycler.setAdapter(adapter);



    }

    private void initializeModel() {
        list = new ArrayList<>();
        list.add(new SettingsModel(0,getString(R.string.nature),R.color.settings_nature));
        list.add(new SettingsModel(1,getString(R.string.friends),R.color.settings_friends));
        list.add(new SettingsModel(2,getString(R.string.default_type),R.color.settings_default));
        list.add(new SettingsModel(3,getString(R.string.food),R.color.settings_food));
    }

}
