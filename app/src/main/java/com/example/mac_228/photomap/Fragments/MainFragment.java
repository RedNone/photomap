package com.example.mac_228.photomap.Fragments;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mac_228.photomap.Activity.MainActivity;
import com.example.mac_228.photomap.Adapter.PagerAdapter;
import com.example.mac_228.photomap.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment implements TabLayout.OnTabSelectedListener {


    public static final String TAG = "MainFragment";

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private PagerAdapter adapter;

    private FragmentManager fragmentManager;

    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main, container, false);

        fragmentManager = ((MainActivity) getActivity()).fragmentManager;

        tabLayout = (TabLayout) view.findViewById(R.id.tabLayout);

        tabLayout.addTab(tabLayout.newTab().setText(R.string.map));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.timeLine));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = (ViewPager) view.findViewById(R.id.pager);

        adapter = new PagerAdapter(getChildFragmentManager(), tabLayout.getTabCount());

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        //Adding onTabSelectedListener to swipe views
        tabLayout.setOnTabSelectedListener(this);


        return view;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


}
